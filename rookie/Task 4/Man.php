<?php

/**
 * Класс Человек
 *
 * Class Man
 */
class Man
{
    static private $count_all_mans = 0;

    /**
     * Данные человека
     * @var array
     */
    private $data;

    /**
     * Энергия, тратится при действиях, восполняется во время сна
     * @var
     */
    private $energy;

    /**
     * Man constructor.
     * @param $name
     */
    public function __construct($name)
    {
        $this->name = $name;
        self::$count_all_mans++;
    }

    public static function getCountAllMans(): int
    {
        return self::$count_all_mans;
    }

    /**
     * Вызывается при записи в недоступное свойство
     *
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    /**
     * Вызывается при чтении из недоступного свойства
     *
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->data[$name];
    }

    /**
     * Проверка на существование недоступного свойства
     *
     * @param $name
     * @return bool
     */
    public function __isset($name)
    {
        return isset($data[$name]);
    }

    /**
     * Unset недоступного свойства
     *
     * @param $name
     */
    public function __unset($name)
    {
        unset($this->data[$name]);
    }

    public function __toString()
    {
        return print_r($this->data, true);
    }

    /**
     * Вызывается при serialize
     */
    public function __sleep()
    {

    }

    /**
     * Вызывается при unserialize
     */
    public function __wakeup()
    {
        $this->energy = 0;
    }

    public function __invoke()
    {
        echo 'Пойду работать';
    }

    /**
     * Вызывается при клонировании объекта
     */
    public function __clone()
    {
        self::$count_all_mans++;
    }

    /**
     * При удалении объекта
     */
    public function __destruct()
    {
        self::$count_all_mans--;
    }
}

$adam = new Man('Адам');
$eva = new Man('Ева');

echo Man::getCountAllMans();



